from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, ReplyKeyboardMarkup, KeyboardButton
from telegram.ext import CommandHandler, Application, CallbackContext, MessageHandler, filters, CallbackQueryHandler
import logging
import traceback
import telegram.error
import requests
from translate import Translator
from random import randint

translator = Translator(to_lang="Ru")

rps_dict = {
    1 : "Rock",
    2 : "Paper",
    3 : "Scissors"
}
api_key_weather = ""
Token = ""

logging.basicConfig(filename='spread_bot.log', format='%(asctime)s %(levelname)-8s %(message)s', level=logging.ERROR,
                    datefmt='%Y-%m-%d %H:%M:%S')


engine = create_engine('sqlite:///DataBase.db', echo=True)

Base = declarative_base()

Session = sessionmaker(bind=engine)
session = Session()

class User(Base):
    __tablename__ = 'users'
    user_id = Column(Integer, primary_key=True)
    user_name = Column(String)

    def __init__(self, user_id, user_name):
        self.user_id = user_id
        self.user_name = user_name

    def __repr__(self):
        return "<User('%s','%s')>" % (self.user_id, self.user_name)



async def SendQ(context: CallbackContext):
    try:
        markup = InlineKeyboardMarkup([[InlineKeyboardButton("Give me 5 minutes...", callback_data="MoreTime"), InlineKeyboardButton("I'm already up!", callback_data="JustWake")]])
        await context.bot.send_message(chat_id=context.job.chat_id, text="Wake up, Neo!", reply_markup=markup, parse_mode="HTML", disable_web_page_preview=True)
    except telegram.error.Forbidden:
        pass
    except Exception:
        logging.error(traceback.format_exc())

async def AddUser(user):
    session.add(user)
    session.commit()

async def start_message(update: Update, context: CallbackContext):
    try:
        UId = update.message.chat.id
        UName = update.message.chat.username
        SqlUser = session.query(User).filter_by(user_id=update.message.chat.id).first()
        if (SqlUser is None):
            user = User(UId, UName)
            await AddUser(user)
            await context.bot.send_message(update.message.chat.id, "Hello!")
        else:
            await context.bot.send_message(update.message.chat.id, "Ты уже зарегистрирован в боте!")
    except Exception:
        logging.error(traceback.format_exc())

async def get_message(update: Update, context: CallbackContext):
    # print((update.message.text,))
    cur_mess = update.message.text

async def do_alarm(update: Update, context: CallbackContext):
    try:
        time_for_sleep = int(update.message.text.split()[1])
        context.job_queue.run_once(SendQ, time_for_sleep, chat_id=update.message.chat_id)
    except Exception as ex:
        pass

async def do_translate(update: Update, context: CallbackContext):
    text = update.message.text[11:]
    translation = translator.translate(text)
    await context.bot.send_message(update.message.chat.id, translation)

async def do_rps(update: Update, context: CallbackContext):
    await context.bot.send_message(update.message.chat.id, rps_dict[randint(1, 3)])

async def do_calc(update: Update, context: CallbackContext):
    text = update.message.text[5:]
    try:
        res = eval(text)
        print(res)
        await context.bot.send_message(update.message.chat.id, str(res))
    except Exception as ex:
        await context.bot.send_message(update.message.chat.id, "Bad input!")


async def do_weather(update: Update, context: CallbackContext):
    url = "https://api.openweathermap.org/data/2.5/weather?lat=55.45&lon=37.37&appid=" + api_key_weather
    res = requests.get(url).json()
    cur = res["weather"][0]['main']
    temp = res['main']['temp']
    await context.bot.send_message(update.message.chat.id, f"At now in Moscow {cur} and {temp} by F!")


async def answer(update: Update, context: CallbackContext):
    print(update.callback_query.data)
    await context.bot.delete_message(update.callback_query.message.chat.id, update.callback_query.message.message_id)
    ans = update.callback_query.data
    if ans == "MoreTime":
        context.job_queue.run_once(SendQ, 5 * 60, chat_id=update.callback_query.message.chat_id)
        await context.bot.send_message(update.callback_query.message.chat.id, "Ok!")
    elif ans == "JustWake":
        await context.bot.send_message(update.callback_query.message.chat.id, "Good morning!")


if __name__ == "__main__":
    try:
        Base.metadata.create_all(engine)

        application = Application.builder().token(Token).build()

        start_handler = CommandHandler('start', start_message)
        alarm_handler = CommandHandler('alarm', do_alarm)
        weather_handler = CommandHandler('weather', do_weather)
        translate_handler = CommandHandler('translate', do_translate)
        calc_handler = CommandHandler('calc', do_calc)
        rps_handler = CommandHandler('rps', do_rps)
        message_handler = MessageHandler(filters.TEXT, get_message)
        # file_handler = MessageHandler(filters.ALL, get_file)
        button_handler = CallbackQueryHandler(answer)

        # application.add_handlers([start_handler, message_handler, button_handler, file_handler])
        application.add_handlers([start_handler, button_handler, alarm_handler, rps_handler,calc_handler, translate_handler, weather_handler, message_handler])
        application.run_polling(timeout=10)
    except Exception as ex:
        print(ex)
        logging.error(traceback.format_exc())
